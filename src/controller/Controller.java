/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author michael.yunker
 */
public class Controller {

    /**
     * @param args the command line arguments
     */
    
    public static void main(String[] args) {
       
      AtomicBoolean up = new AtomicBoolean(true);
       AtomicBoolean down = new AtomicBoolean(true);
        AtomicBoolean left = new AtomicBoolean(true);
      AtomicBoolean right = new AtomicBoolean(true);
       AtomicBoolean A = new AtomicBoolean(true);
        AtomicBoolean B = new AtomicBoolean(true);
       
        new Thread()
        {
           
           public void run()
            {
                 try 
                 {
                    ServerSocket serverSocket = new ServerSocket(4001);
                    Socket clientSocket = serverSocket.accept();
                    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    String input;
                    Robot robot = new Robot(); 
                   
                
                    while(!(input=in.readLine()).equals("kill"))
                    {
                        if(input.equals("Wd"))
                        {
                            
                            up.set(true);
                            new Thread()
                            {
                                public void run()
                                {
                                    while(up.get())
                                    {
                                        robot.keyPress(KeyEvent.VK_W);
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    return;
                                }
                            }.start();
                        }
                        else if(input.equals("Wu"))
                        {
                            up.set(false);
                            robot.keyRelease(KeyEvent.VK_W);
                        }
                        
                        else if(input.equals("Dd"))
                        {
                            right.set(true);
                            new Thread()
                            {
                                public void run()
                                {
                                    while(right.get())
                                    {
                                        robot.keyPress(KeyEvent.VK_D);
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    return;
                                }
                            }.start();
                        }
                        else if(input.equals("Du"))
                        {
                            right.set(false);
                            robot.keyRelease(KeyEvent.VK_D);
                        }
                        
                        else if(input.equals("Sd"))
                        {
                            down.set(true);
                            new Thread()
                            {
                                public void run()
                                {
                                    while(down.get())
                                    {
                                        robot.keyPress(KeyEvent.VK_S);
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    return;
                                }
                            }.start();
                        }
                        else if(input.equals("Su"))
                        {
                            down.set(false);
                            robot.keyRelease(KeyEvent.VK_S);
                        }
                        
                         else if(input.equals("Ad"))
                        {
                            left.set(true);
                            new Thread()
                            {
                                public void run()
                                {
                                    while(left.get())
                                    {
                                        robot.keyPress(KeyEvent.VK_A);
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    return;
                                }
                            }.start();
                        }
                        else if(input.equals("Au"))
                        {
                            left.set(false);
                            robot.keyRelease(KeyEvent.VK_A);
                        }
                        
                         else if(input.equals("Zd"))
                        {
                            A.set(true);
                            new Thread()
                            {
                                public void run()
                                {
                                    while(A.get())
                                    {
                                        robot.keyPress(KeyEvent.VK_Z);
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    return;
                                }
                            }.start();
                        }
                        else if(input.equals("Zu"))
                        {
                            A.set(false);
                            robot.keyRelease(KeyEvent.VK_Z);
                        }
                        
                         else if(input.equals("Xd"))
                        {
                            B.set(true);
                            new Thread()
                            {
                                public void run()
                                {
                                    while(B.get())
                                    {
                                        robot.keyPress(KeyEvent.VK_X);
                                        try {
                                            Thread.sleep(200);
                                        } catch (InterruptedException ex) {
                                            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                    return;
                                }
                            }.start();
                        }
                        else if(input.equals("Xu"))
                        {
                            B.set(false);
                            robot.keyRelease(KeyEvent.VK_X);
                        }
                        else if(input.equals("Qd"))
                        {
                          
                            robot.keyPress(KeyEvent.VK_Q);
                            Thread.sleep(25);
                            robot.keyRelease(KeyEvent.VK_Q);
                       
                        }
                        else if(input.equals("Ed"))
                        {
                          
                            robot.keyPress(KeyEvent.VK_E);
                            Thread.sleep(25);
                            robot.keyRelease(KeyEvent.VK_E);
                       
                        }
                        
                        
                    }
                } catch (Exception e) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, e);
                }
                }
        }.start();
        }
    

    }
    

